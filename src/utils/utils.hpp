#pragma once

#include <random>

namespace utils {

double random(double max, double min);

}
