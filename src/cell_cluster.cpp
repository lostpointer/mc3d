#include "cell_cluster.h"

#include <utils/logger.hpp>
#include <utils/stopwatch.hpp>

using namespace std;
using namespace mc3d;

namespace mc3d {
/*double V(double x, double y, double R)
{
  return 0.3 * (x * x + y * y) * std::exp(0.5 * (1 - (x * x + y * y) / (R * R)))
/ R;
}*/

CellCluster::CellCluster(void) : thread_pool_(NUM_CPU, "move_and_collisions") {
  Kn = 0;
  np = 0;
  ncx = 0;
  ncy = 0;
  ncz = 0;
  N = 0;
  Lx = 0;
  Ly = 0;
  Lz = 0;
  t = 0;
  dt = 10000;
  step = 0;
}

CellCluster::~CellCluster(void) { cells.clear(); }

bool CellCluster::initialazition(unsigned int ncx, unsigned int ncy,
                                 unsigned int ncz, double density, double Kn,
                                 double Cu, std::unique_ptr<geometry>&& body,
                                 double S, double alpha, double T) {
  LOG_DEBUG() << bool(body);
  body_ = std::move(body);

  srand(static_cast<unsigned int>(time(nullptr)));

  this->ncx = ncx;
  this->ncy = ncy;
  this->ncz = ncz;

  this->density = density;

  this->Kn = Kn;
  this->Cu = Cu;

  unsigned int N = 0;

  double dx = Lx / double(ncx);
  double dy = Ly / double(ncy);
  double dz = Lz / double(ncz);

  this->t = 0;
  this->dt = 1000000000;

  auto cell_iter = cells.begin();
  for (size_t i = 0; i < ncx; i++) {
    for (size_t j = 0; j < ncy; j++) {
      for (size_t k = 0; k < ncz; k++) {
        cell temp_cell;

        size_t N_ = 0;
        point a;
        a.x = apex.x + i * dx;
        a.y = apex.y + j * dy;
        a.z = apex.z + k * dz;

        if (i == ncx - 1)
          N_ = static_cast<unsigned int>(density * dx * dy * dz);
        temp_cell.set_param(S, alpha, T);

        temp_cell.set_apex(a);
        temp_cell.set_size(dx, dy, dz);

        temp_cell.initialazition(N_, body_);

        temp_cell.set_L(Lx);
        temp_cell.set_Kn(Kn);

        N += N_;
        double dtt = temp_cell.get_dt();
        dt = min(dt, dtt);

        cells.emplace_back(std::move(temp_cell));
      }
    }
  }

  //  cells_test();

  this->N = N;

  this->density = N;

  cell_iter = cells.begin();
  for (auto& cell : cells) {
    cell.clean_inner_particle(*body_);
  }

  this->sync_dt();
  cout << "Cluster initialize is done" << endl;

  return true;
}

void CellCluster::set_apex(point apex) { this->apex = apex; }

void CellCluster::set_size(double Lx, double Ly, double Lz) {
  this->Lx = Lx;
  this->Ly = Ly;
  this->Lz = Lz;
}

bool CellCluster::time_step() {
  sync_dt();

  LOG_INFO() << "dt = " << dt << " t = " << t;

  //  auto cell_iter = cells.begin();

  std::vector<std::future<void>> futures;
  futures.reserve(cells.size());
  for (auto& cell : cells) {
    futures.emplace_back(thread_pool_.Execute([&cell]() { cell.calc(); }));
  }
  for (auto& fut : futures) {
    fut.get();
  }
  //  while (cell_iter != cells.end()) {
  //    std::vector<std::thread> threads;
  //    threads.reserve(NUM_CPU);
  //    for (size_t i = 0; i < NUM_CPU; i++) {
  //      if (cell_iter == cells.end()) break;
  //      threads.emplace_back([cell_iter]() { cell_iter->calc(); });
  //      cell_iter++;
  //    }
  //    for (auto& thread : threads) thread.join();
  //  }

  for (auto& cell : cells) {
    //(*cell_iter)->_dbg_test_particle();
    cell.calc_vel();
    cell.sort();
    cell.neighbor_sort();
    auto cell_buffer = cell.get_buffer();
    partile_buffer.insert(partile_buffer.end(), cell_buffer->begin(),
                          cell_buffer->end());
    cell_buffer->clear();
  }

  for (auto& cell : cells) {
    cell.add_particle(&partile_buffer);
  }

  boundary_condition();

  for (auto& cell : cells) {
    cell.add_particle(&partile_buffer);
  }
  t += dt;

  partile_buffer.clear();

  std::string file_name = "data";
  std::ostringstream ost;
  ost << t << ".dat";
  file_name += ost.str();
  data_t += data_dt;
  WriteFile(file_name);

  if (t >= t_end)
    return true;
  else
    return false;
}

void CellCluster::boundary_condition() {
  deque<boundary*>::iterator a = boundary_cond_outer.begin();
  while (a != boundary_cond_outer.end()) {
    (*a)->bondary_condition(&partile_buffer, dt);
    a++;
  }
}

bool CellCluster::send_data() { return true; }

bool CellCluster::recv_data() { return true; }

bool CellCluster::WriteFile(const std::string& file_name) {
  std::ofstream file1(file_name);
  cell_iter = cells.begin();
  point ap;
  unsigned int N;
  file1 << "x;y;z;N;ro;T;vx;vy;vz;E" << endl;
  for (auto& cell : cells) {
    auto ap = cell.get_center();
    auto vel = cell.get_velocity();
    N = cell.N();
    file1 << ap.x << ";" << ap.y << ";" << ap.z << ";" << N << ";"
          << double(N) / (cell.get_volume() * density) << ";" << cell.get_t()
          << ";" << vel.x << ";" << vel.y << ";" << vel.z << ";"
          << cell.get_energy() << endl;
    cell_iter++;
  }
  file1.close();
  return true;
}

bool CellCluster::write_speed_file(const char* file_name) {
  std::ofstream file1(file_name);
  cell_iter = cells.begin();
  point ap;
  unsigned int N;
  for (auto& cell : cells) {
    ap = cell.get_center();
    N = cell.N();
    if (double(N) / (cell.get_volume() * density) < 0) {
      bool a = body_->is_inner_point(ap);
      a = body_->is_inner_point(cell.get_apex());
    }
    file1 << ap << cell.get_velocity() << endl;
    cell_iter++;
  }
  file1.close();
  return true;
}

bool CellCluster::WriteFile() {
  std::ofstream file1("data.dat");
  cell_iter = cells.begin();
  unsigned int N;
  double av_den = 0;
  point cell_size;
  double volume = 1;
  for (auto& cell : cells) {
    auto ap = cell.get_center();
    av_den += double(N) / np;
    file1 << ap << "\t" << double(cell.N()) / (cell.get_volume() * density)
          << "\t" << cell.get_t() << endl;
    cell_iter++;
  }
  file1.close();
  return true;
}

bool CellCluster::write_speed_file() {
  std::ofstream file1("speed.dat");
  cell_iter = cells.begin();
  point ap;
  unsigned int N;
  double av_den = 0;
  for (auto& cell : cells) {
    ap = cell.get_center();
    N = cell.N();
    av_den += double(N) / np;
    file1 << ap << "\t" << cell.get_velocity() << endl;
    cell_iter++;
  }

  file1.close();
  return true;
}

bool CellCluster::write_times() {
  if (proc_id == 0) {
    std::ofstream times_file("time.dat");

    deque<double>::iterator time_iter = times.begin();

    int i = 0;

    while (time_iter != times.end()) {
      times_file << i << "\t" << times[i] << "\t" << sort_times[i] << "\t"
                 << send_times[i] << "\t" << bound_times[i] << "\t"
                 << calc_times[i] << endl;

      i++;
      time_iter++;
    }

    times_file.close();
  }
  return 0;
}

void CellCluster::set_boundary_condition(boundary** a, int n) {
  for (int i = 0; i < n; i++) {
    boundary_cond_outer.push_back(a[i]);
  }

  //добавляем ссылки на ячейки в свободные границы
  deque<boundary*>::iterator bc = boundary_cond_outer.begin();
  while (bc != boundary_cond_outer.end()) {
    if (typeid(**bc) == typeid(FreeBoundary)) {
      FreeBoundary* b = dynamic_cast<FreeBoundary*>(*bc);
      b->add_cell(cells);
      // b->set_np(np);
    } else if (typeid(**bc) == typeid(giper_free_boundary)) {
      FreeBoundary* b = dynamic_cast<FreeBoundary*>(*bc);
      b->add_cell(cells);
      // b->set_np(np);
    }

    bc++;
  }
}

void CellCluster::set_boundary_condition(boundary* a) {
  if (typeid(a) == typeid(FreeBoundary)) {
    FreeBoundary* b = dynamic_cast<FreeBoundary*>(a);
    b->add_cell(cells);
  }

  boundary_cond_outer.push_back(a);
}

void CellCluster::computation(void) {
  size_t step = 0;
  while (t < t_end) {
    LOG_INFO() << "Time step:" << ++step;
    utils::Stopwatch("ClusterStep"), this->time_step();
  }
}

void CellCluster::set_end_time(double t_end) {
  this->data_t = 0;
  this->data_dt = t_end / 100 + 0.000001;
  this->t_end = t_end;
}
void CellCluster::calc_dt() {
  dt = 1000000.;

  cell_iter = cells.begin();
  for (auto& cell : cells) {
    double dtt = cell.calc_dt();
    if (dtt < dt) dt = dtt;
  }
  dt *= Cu;
  if (t + dt > t_end) dt = t_end - t + 0.000000001;
}

void CellCluster::set_dt_in_cells(double dt) {
  for (auto& cell : cells) {
    cell.set_dt(dt);
  }
}

void CellCluster::sync_dt() {
  calc_dt();
  set_dt_in_cells(dt);
}

void CellCluster::sync_data() {}

void CellCluster::cells_fragmentation() {}

void CellCluster::cells_test() {
  deque<cell*> cells_swap;

  cell_iter = cells.begin();
  while (cell_iter != cells.end()) {
    cell_iter++;
  }
}

bool CellCluster::write_cell_file(const std::string& init_file) {
  std::ofstream file(init_file);
  if (!file.is_open()) {
    LOG_ERROR() << "Can't open file: " << init_file;
    return false;
  }

  file << "cluster" << std::endl;

  cell_iter = cells.begin();
  for (auto& cell : cells) {
    point V = cell.get_velocity();
    double S = sqrt((V.x * V.x + V.y * V.y) / 2 * cell.get_t());
    double alpha = 3.141592654 / 2 - std::atan(V.x / V.y);
    if (V.x > 100000 * V.y) {
      V.x > 0 ? alpha = 0 : alpha = 3.141592654;
    }

    file << "\tcell" << endl;

    file << "\t\tapex\t" << cell.get_apex() << endl;

    file << "\t\tsize\t" << cell.get_size() << endl;

    file << "\t\tS\t" << S << endl;

    file << "\t\talpha\t" << alpha << endl;

    file << "\t\tparticles\t" << cell.N() << endl;

    file << "\t\ttemperature\t" << cell.get_t() << endl;

    file << "\t\tL\t" << cell.get_L() << endl;

    file << "\tendcell" << endl;

    cell_iter++;
  }

  return true;
}

void CellCluster::set_data_save_dtime(double data_dt) {
  this->data_dt = data_dt;
}

void CellCluster::clean_inner_particle() {
  for (auto& cell : cells) {
    cell.clean_inner_particle(*body_);
  }
}
}  // namespace mc3d
